$(document).ready(function(){


//Set variables using elements
var login_input = ".login-input";
var content = "#content";
var link_button = ".link_button";
var frm_login = "#formLogin";
var login_button = "#loginButton";
var login_id = "#loginID";
var login_pass = "#loginPass";
var page = "#page";
var tabs = ".header-menu";
var search_box = "#input-search-box";
var search_cover = "#search-users-border";
var online_user = "#online-users-cover";
var dimmer = "#screen_dimmer";
var loader = "#request_loader";
var load_text = "#request_name";
var frmReg = "#frmRegister";
var username = "#username";
var email = "#email";
var userPass = "#userPass";
var confirmPass = "#confirmPass";
var regBtn = "#submitRegButton";
var search_button = "#top-search";
var search_box = "#search_text";
var open_setting = "#top-settings";


//Set default match maker profile
var matchMaker = storage.get("matchMaker") || {};

if($.isEmptyObject(matchMaker)){
    
    var matchMaker = {}

    matchMaker.about = "";
    matchMaker.myGender = "Male";
    matchMaker.birthDate = "1997/03/19";
    matchMaker.race = "African";
    matchMaker.haveChild = "No";
    matchMaker.numberChild = 0;
    matchMaker.wantChild = "Yes";
    matchMaker.marital = "Single";
    matchMaker.myLanguage = "Tshivenda";
    matchMaker.eatHabit = "Healthy";
    matchMaker.uDrink = "No";
    matchMaker.uSmoke = "No";
    matchMaker.bodyType = "Slim";
    matchMaker.hairColor = "black";
    matchMaker.eyeColor = "black";
    matchMaker.lookFor = "Male";
    matchMaker.lookRace = "African";
    matchMaker.lookMarital = "Single";
    matchMaker.lookHaveChild = "Yes";
    matchMaker.lookEatHabit = "Healthy";
    matchMaker.lookBodyType = "Slim";
    matchMaker.lookHairColor = "black";
    matchMaker.lookEyeColor = "black";
    matchMaker.lookCanDrink = "No";
    matchMaker.lookCanSmoke = "No";

    storage.set("matchMaker", matchMaker);

}



//Set notifications sounds
$(document).on("click", "#alertSound", function(){

    if($(this).is(":checked")){
        storage.set("sound", true);
    }else{
        storage.set("sound", false);
    }


})

//Set notifications vibrations
$(document).on("click", "#alertVibrate", function(){

    if($(this).is(":checked")){
        storage.set("vibrate", true);
    }else{
        storage.set("vibrate", false);
    }


})


//Open TapHoldMenu
$(document).on("click", ".fav-users-cover", function(){

    var user = $(this).children(".chat_username").text()
    var profile = $(this).children(".chat_user_pic").children('img').attr('src');
    var about = $(this).children(".user-fav-review").text();
    var table_name = storage.get(user) || [];

    storage.set("menu", "popMenu");

    $("#openConvo").attr({'data-user' : user, 'data-profile' : profile });
    $("#view_profile").attr({'data-user' : user, 'data-profile' : profile, 'data-about' : about });

    if(table_name.length > 0){
        $("#openConvo").text("Open Conversation");
        $("#deleteConvo").show();
    }else{
        $("#openConvo").text("Start Conversation");
        $("#deleteConvo").hide();
    }


    $("#deleteConvo").attr("data-user", user);
    $("#deleteUser").attr("data-user", user);
    $("#tapHoldMenu").fadeIn("slow");
})


//Close TapHoldMenu
$(document).on("click", "#tapHoldMenu", function(){
    $(this).fadeOut("slow");
    storage.set("menu", "favorites");
})

//Stop bubbling
$(document).on("click", "#chatsHoldsMenu, #showEmoticons, .emoticons", function(e){
    e.stopPropagation();
})

//Cancel space entries in username
$(document).on("keyup", username, function(){

    var newString = $(this).val().split(' ').join('');
    $(this).val(newString);

})

//Validate email address
$(document).on("blur", email, function(){

    if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*.(\.\w{2,3})+$/.test($(this).val()) == false){
        $(load_text).html("Invalid email address").attr("class", "error_login");
        $(loader).hide();
        $(dimmer).fadeIn("slow");
        $(dimmer).delay(2000).fadeOut("slow", function(){
            $(load_text).attr("class", "request_name").html("Signing in...");
            $(loader).show();
        });
    }

})

//Validate password requirements
$(document).on("blur", userPass, function(){

    if($(this).val().length < 6){
        $(load_text).html("Your password is too weak").attr("class", "error_login");
        $(loader).hide();
        $(dimmer).fadeIn("slow");
        $(dimmer).delay(2000).fadeOut("slow", function(){
            $(load_text).attr("class", "request_name").html("Signing in...");
            $(loader).show();
        });
    }

})


//Validate password matching
$(document).on("blur", confirmPass, function(){

    if($(this).val() != $(userPass).val()){
        $(load_text).html("Password mismatch").attr("class", "error_login");
        $(loader).hide();
        $(dimmer).fadeIn("slow");
        $(dimmer).delay(2000).fadeOut("slow", function(){
            $(load_text).attr("class", "request_name").html("Signing in...");
            $(loader).show();
        });
    }

})



//Change pages of the app
$(document).on("click", link_button, function(){

    var open_page = $(this).attr("data-open");
    var current_tab = $(this);

    $(content).empty();
    $(content).load("pages/" + open_page, function(){
        $(tabs).removeClass("active-tab");
        $(current_tab).addClass("active-tab");

        if($("#display_chats").length){
             var max_height = $(document).height() - 90;
            $("#display_chats").css({ "max-height" : max_height, "overflow-y" : "auto" });
            storage.set("menu", "home");
        }

        if($("#display_favorites").length){
             var max_height = $(document).height() - 90;
            $("#display_favorites").css({ "max-height" : max_height, "overflow-y" : "auto" });
            storage.set("menu", "favorites");
        }

        if($("#hookups").length){
            
            $("#loadHooker").show();
            var tbl_sugg = storage.get("suggestions") || [];
            var tbl_fav = storage.get("favorites") || [];
            var table_dislike = storage.get('dislikes') || [];
            var max_height = $(document).height() - 90;
            $("#hookups").css({ "height" : max_height, "overflow-y" : "auto" });
            var HTMLStr = "";

            storage.set("menu", "matches");

            //Display first suggest
            if(tbl_sugg.length > 0){

                loopSugg: for(var i in tbl_sugg){
                    
                    if(table_dislike.indexOf(tbl_sugg[i].username) !== -1) {
                        tbl_sugg.splice(i, 1);
                        continue;
                    }

                    for(m in tbl_fav){
                        if(tbl_sugg[i].username == tbl_fav[m].username){
                            continue loopSugg;                           
                        }
                    }

                    var gender = (tbl_sugg[i].gender !== "") ? tbl_sugg[i].gender : "";
                    var profile = (tbl_sugg[i].profile !== "") ? tbl_sugg[i].profile : "";
                    var age = (tbl_sugg[i].age !== "") ? ", " + tbl_sugg[i].age + " years" : "";
                    var about = (tbl_sugg[i].about === "") ? "not available" : tbl_sugg[i].about;

                    HTMLStr += "<div class='matches_cover' data-about='" + about + "' data-gender='" + gender + age + " ' data-profile='" + tbl_sugg[i].profile + "' data-user='" + tbl_sugg[i].username + "'>"+
                             "<div class='chat_user_pic'>" +
                             "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_sugg[i].profile + "' />"+
                             "</div>" + 
                             "<div class='chat_username'>" + tbl_sugg[i].username +  "</div>"+
                             "<div class='hook_gender_age'>" +  gender + age + "</div>"+ 
                             "</div>";  
                                                   

                }

                $("#loadHooker").hide();    
                $("#hookups").append(HTMLStr);            
            }else{

                $("#loadHooker").hide();
                HTMLStr += "<em>No suggestions found. <br /><br />Please ensure that you filled your match marker form</em>";
                $("#hookups").append(HTMLStr);
            }

        }


    });
    

})



//Open search box
$(document).on("click", "#searchOpen", function(){

    $("#searchUserLoader").fadeIn("slow", function(){

        var max_height = $(document).height() - 55;
        $("#searchresultsDisplay").height(max_height);

    });

})


//Close search box
$(document).on("click", "#closeSearchBox", function(){

    $("#searchUserLoader").fadeOut("fast");

})

//View user profile
$(document).on("click", ".matches_cover", function(){

    var _this = $(this);

    $("#view-user-details").load("pages/profile.html");
    
    $("#view-user-details").fadeIn("fast", function(){

        var user = _this.attr("data-user");
        var profile = _this.attr("data-profile");
        var gender = _this.attr("data-gender");
        var aboutMe = _this.attr("data-about");
        var limit = 200;

        storage.set('user_invite', user);
        storage.set('profile_invite', profile);
        storage.set('about', aboutMe);
        storage.set('menu', "profile");

        $("#view_profile_username").text(user);
        $("#view_profile_picture img").attr("src", "http://www.lovenest.co.za/profile_pics/" + profile);
        $("#view_profile_gender").text(gender);

        var total = aboutMe.length;

        var btnReadMore = "<span class='btnReadMore'> ...View All</span>";

        if(total > limit){
            aboutMe = aboutMe.substr(0, limit);
            $("#view_profile_about_heading").html("About " + user);
            $("#view_profile_about").html(aboutMe + btnReadMore);
        }else{
            $("#view_profile_about_heading").html("About " + user);
            $("#view_profile_about").html(aboutMe);
        }


        $(document).on("click", ".btnReadMore", function(){
            $("#view_profile_about_heading").html("About " + user);
            $("#view_profile_about").html(aboutMe);
        })

    });

})



//Close view profil pop
$(document).on("click", "#go_back_hooks", function(){
    $("#view-user-details").fadeOut("slow", function(){
        $("#likedOrDislike").hide();
        $("#like-button").show();
        $("#dislike-button").show();
    });
})


//Go to home page
$(document).on("click", ".user_name_view", function(){
    
    $("#pop_chats").animate({ "top" : "25%", "left" : "25%" }, "slow").hide().empty();
     
})

//Display chats
function display_chats(){
  var tbl_chats = storage.get("chats") || [];
    var HTMLStr = "";

    if(tbl_chats.length === 0){
        HTMLStr += "<em>No previous chats<br /><br />Go to favorites and start conversation</em>";

    }else{
        for(var i in tbl_chats){

            if("read" in tbl_chats[i]){
                

                var WhenTime =  tbl_chats[i].time;

                if(calculateDays(tbl_chats[i].date) === 1){
                    WhenTime = "Yesterday";
                }else if(calculateDays(tbl_chats[i].date) > 1){
                    WhenTime = tbl_chats[i].date;
                }

                var statusTime = (tbl_chats[i].read === 0) ? "<span style='color: #F00'>Unread</span>" : WhenTime;
                var onlineStatus = (tbl_chats[i].status === "") ? "<div class='onlineIcons'></div>" : "";

                HTMLStr += "<div class='online-users-cover'><div class='chat_user_pic'>" +
                             "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_chats[i].profile + "' />"+
                             "</div>" + 
                             "<div class='chat_username'>" + onlineStatus  + tbl_chats[i].username +  "</div>"+
                             "<div class='unread'>" + statusTime +  "</div>"+
                             "<div class='review_message incomings'>" +  tbl_chats[i].msg + "</div>"+ 
                             "</div>";           
            }else{
                var sent = (tbl_chats[i].seen === 0) ? "..." : "<img src='img/tick.png' style='width: 12px' />";              
                var onlineStatus = (tbl_chats[i].status === "") ? "<div class='onlineIcons'></div>" : "";

                var WhenTime =  tbl_chats[i].time;

                if(calculateDays(tbl_chats[i].date) === 1){
                    WhenTime = "Yesterday";
                }else if(calculateDays(tbl_chats[i].date) > 1){
                    WhenTime = tbl_chats[i].date;
                }

                HTMLStr += "<div class='online-users-cover'><div class='chat_user_pic'>" +
                             "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_chats[i].profile + "' />"+
                             "</div>" + 
                             "<div class='chat_username'>" + onlineStatus  + tbl_chats[i].username +  "</div>"+
                             "<div class='unread'>" + WhenTime +  "</div>"+ 
                             "<div class='review_message'>" +  sent + " <span class='chatsMessageReview'>" + tbl_chats[i].msg + "</span></div>"+ 
                             "</div>"; 


            }

        }
    }



        if($("#display_chats").length){

            var max_height = $(document).height() - 70;
            $("#display_chats").css({ "height" : max_height, "overflow-y" : "auto" });
            $("#display_chats").html(HTMLStr);

        }
        

}


//Stop bubbling
$(document).on("click", "#settings_content", function(e){
     e.stopPropagation();
})

$(document).on("click", function(){
    $("#settings_cover").fadeOut("fast");
 
})


//Open settings drop down
$(document).on("click", "#top-settings", openMenu);
document.addEventListener("menubutton", openMenu, false);


function openMenu(e){
    e.stopPropagation();
    $("#settings_cover").fadeIn("slow");
    storage.set("menu", "options"); 
}


//Open tabs in a profile view
$(document).on("click", ".category_heading", function(){

    $(".close_category_heading").not($(this))
                                .siblings(".category_body")
                                .fadeOut("fast");
    $(".close_category_heading").attr("class", "category_heading");
    $(this).attr("class", "close_category_heading");
    $(this).siblings(".category_body").fadeIn("fast");
})


//Close opened tab in a profile view
$(document).on("click", ".close_category_heading", function(){
    $(this).siblings(".category_body").fadeOut("fast");
    $(this).attr("class", "category_heading");
})


//Open chat conversation
$(document).on("click", ".online-users-cover", function(){

    if(storage.get("menu") === "options") return;

    var user = $(this).children(".chat_username").text();
    var chat_user = user.replace(/\s+/g, '');
    var picture = $(this).children(".chat_user_pic").children('img').attr('src');
    var table_name = storage.get(chat_user);
    var max_height = $(document).height() - 92;
    var HTMLStr = "";

    storage.set("menu", "conversation");

    $("#pop_chats").show().animate({ "top" : 0, "left" : 0, "right" : 0, "bottom" : 0 }, "fast", function(){

        $(this).load("pages/conversation.html", function(){

            $(".view_username").html(user);
            $(".view_profile img").attr("src", picture);
            $('.conversation_body').attr('data-active-user', user);
            $('.conversation_body').css({'height' : max_height, 'overflow-y' : 'auto'});

            for(var i in table_name){

                if("seen" in table_name[i]){

                    var sent = (table_name[i].seen == 0) ? "<img src='img/notSent.png' style='width: 12px' />" : "<img src='img/tick.png' style='width: 12px' />";
                    var not_sent = (table_name[i].seen == 0) ? "sent_pending" : "";
                    
                    HTMLStr += "<div class='conversation_outgoing " + not_sent + "'>"+
                         "<div class='outgoing_conversation_cover'>" + table_name[i].message + "</div>" +
                         "<div class='send-time_send-status'>" + table_name[i].send_time + " " + sent + " </div>" +
                         "<br style='clear : both' /></div>";            
                    
                }else if("read" in table_name[i]){
                    table_name[i].read = 1;
                    storage.set(chat_user, table_name);

                    HTMLStr += "<div class='conversation_income'>"+
                         "<div class='income_conversation_cover'>" + table_name[i].message + "</div>" +
                         "<div class='receive-time'>" + table_name[i].send_time + "</div>" + 
                         "<br style='clear : both' /></div>";


                }

            }

            var count = $("#unreadChatCount").text()

            if(count > 0){
                $("#unreadChatCount").text(Number(count - 1));
                $("#unreadChatCount").show();
            }else{
                $("#unreadChatCount").hide();
            }

           
           $(".conversation_body").html(HTMLStr); 
           $(".chat-text-box").focus();

        });

    });



});
    



//Open chat conversation
$(document).on("click", "#openConvo", function(){

    var user = $(this).attr('data-user');
    var chat_user = user.replace(/\s+/g, '');
    var picture = $(this).attr('data-profile');
    var table_name = storage.get(chat_user) || [];
    var max_height = $(document).height() - 92;
    var HTMLStr = "";

    storage.set("menu", "conversation");

    $("#pop_chats").show().animate({ "top" : 0, "left" : 0, "right" : 0, "bottom" : 0 }, "fast", function(){

        $(this).load("pages/conversation.html", function(){
            $(".view_username").html(user);         
            $(".view_profile img").attr("src", picture);
            $('.conversation_body').attr('data-active-user', user);
            $('.conversation_body').css({'height' : max_height});

            
            if(table_name.length === 0){
                $(".startConvo").append(user).show();
                return;
            };

            for(var i in table_name){

                if("seen" in table_name[i]){

                    var sent = (table_name[i].seen == 0) ? "<img src='img/notSent.png' style='width: 12px' />" : "<img src='img/tick.png' style='width: 12px' />";
                    var not_sent = (table_name[i].seen == 0) ? "sent_pending" : "";
                    
                    HTMLStr += "<div class='conversation_outgoing " + not_sent + "'>"+
                         "<div class='outgoing_conversation_cover'>" + table_name[i].message + "</div>" +
                         "<div class='send-time_send-status'>" + table_name[i].send_time + " " + sent + " </div>" +
                         "<br style='clear : both' /></div>";            
                    
                }else if("read" in table_name[i]){
                    table_name[i].read = 1;
                    storage.set(chat_user, table_name);

                    HTMLStr += "<div class='conversation_income'>"+
                         "<div class='income_conversation_cover'>" + table_name[i].message + "</div>" +
                         "<div class='receive-time'>" + table_name[i].send_time + "</div>" + 
                         "<br style='clear : both' /></div>";

                }

            }

            var count = $("#unreadChatCount").text()

            if(count > 0){
                $("#unreadChatCount").text(Number(count - 1));
                $("#unreadChatCount").show();
            }else{
                $("#unreadChatCount").hide();
            }

           
           $(".conversation_body").html(HTMLStr); 
           $(".chat-text-box").focus();

        });

        $("#tapHoldMenu").fadeOut("fast");


    });



});



//Clear conversation
$(document).on("click", ".delete-chat-conversation", function(){
    var user = $(".view_username").text().replace(/\s+/g, '');

    var confirmDelete = confirm("Clear conversation with " + $(".view_username").text() + "?")

    if(!confirmDelete) return;

    $(".conversation_body").html("<div class='startConvo'>Start a conversation with </div>");
    $(".startConvo").show().append($(".view_username").text());
    storage.remove(user);
})

//Clear conversation
$(document).on("click", "#deleteConvo", function(){

    var user = $(this).attr("data-user").replace(/\s+/g, '');
    var deleteConfirm = confirm("Delete conversation with " + user + "?");

    if(deleteConfirm){
        storage.remove(user);
        $("#tapHoldMenu").trigger("click");
    }

})


//Clear conversation
$(document).on("click", "#deleteUser", function(){

    var user = $(this).attr("data-user").replace(/\s+/g, '');
    var fav = storage.get("favorites") || [];
    var deleteConfirm = confirm("Delete user " + user + "?");

    if(deleteConfirm){

        for(i in fav){
            if(fav[i].username === user) {
                storage.remove(user);
                fav.splice(i, 1);
                storage.set("favorites", fav);
                $("#tapHoldMenu").trigger("click");
            }
        }


        

    }

})


//Logging user out
$(document).on("click", "#logout", function(){
    storage.set("isLogin", false);
    window.location = "index.html";
})

//Open menus pop up
$(document).on("click", ".menuOpenPop", function(){

    var link = $(this).attr("data-open");
    var title = $(this).text();
    var icon = $(this).attr("data-icon");

    $(".view_menu_icon").css("background", "url('img/" + icon + "') no-repeat center");
    $(".view_menu_name").text(title);

    $("#menu_pop_body").load("pages/" + link, function(){
        $("#menu_popup").fadeIn("slow");
        $("#settings_cover").fadeOut("slow");
        storage.set("menu", "optionsMenu");

        if($("#viewMyProfile").length){

            var width = $("#viewMyProfile").width();
            var profile = storage.get("profile");
            var tmpImage = storage.get("tempImage");
            var username = storage.get("username");
            var email = storage.get("email");
            var imgData = (tmpImage === null) ? "http://www.lovenest.co.za/profile_pics/" + profile : tmpImage[0].imgData;

            $("#viewMyProfile").css({ "height" : width }) //Set profile pic cover height
            $("#viewMyProfile img").css({ "min-height" : width }).attr("src", imgData) //Set profile pic minimum height
            $("#viewMyProfileUserName").text(username);
            $("#viewMyProfileUserEmail").text(email);
            $("#viewMyProfileEditImage").show();
        }


        if($("#matchMakersCover").length){

            var max_height = $(document).height() - 132;

            $("#matchMakerProfileCover").height(max_height);


               //Insert matchmakers data to the form
               $("#input_aboutme").val(matchMaker.about);
                
               $(".selectCheckBox").each(function(){

                    var val = $(this).attr("data-value");
                    var key = $(this).attr("data-obj");

                    if(val === matchMaker[key]){
                        $(this).prop("checked", true);

                        $(this).parent(".selectInfoCover").parent(".selectYourInfoBody")
                               .siblings(".selectYourInfoHeader")
                               .children(".OptSelectedLabel")
                               .text(val);

                    }

               })

            

        }



            
    })

})



//Close menu pop
$(document).on("click", "#menu_pop_title", function(){

    $("#menu_popup").fadeOut("slow", function(){
        $(".view_menu_name").empty()
        $(".view_menu_icon").show();
        $("#menu_pop_body").empty()
        $("#settings_cover").fadeIn("slow");
        $("#viewMyProfileEditImage").hide();
    });

})


//Edit profile
$(document).on("click", "#viewMyProfileEditImage", function(e){

    $("#viewEditPictureOptions").fadeIn("slow")

    e.stopPropagation();

})




//Settings links
$(document).on("click", ".menu_pop_button", function(){

    var title = $(this).text();
    var link = $(this).attr("data-open");
    var dislikes = storage.get('dislikes') || [];
    var sound = storage.get("sound");
    var vibrate = storage.get("vibrate");
    var icon = $(this).attr("data-icon");

    // $("#menu_pop_title").text(title);
    $(".view_menu_icon").hide();
    $(".view_menu_name").text(title);

    $("#menu_pop_body").load("pages/" + link, function(){
        $("#menu_popup").fadeIn("slow");
        $("#settings_cover").fadeOut("slow");


        if($("#showDislikes").length){

            var list = "";

            for(var i = 0; i < dislikes.length; i++){
                list += "<div class='dislikeList'>" + dislikes[i] + "</div>";

            }

            $("#showDislikes").html(list);
        }


        if($(".notify_buttons").length){ 
          
            if(sound === null || sound === true){
                storage.set("sound", true);
                $("#alertSound").attr("checked", true);
            }

            if(vibrate === null || vibrate === true){
                storage.set("vibrate", true);
                $("#alertVibrate").prop("checked", true);
            }
        }

    })


})


//Open unread messages
$(document).on("click", "#new-chats", function(){

    $("#chat").trigger("click");

})

//Display emoticons
$(document).on("click", ".emoticons", function(){

    var emoticons = ["angel.png", "angry.png", "beer.png", "blush.png", "boy.png", 
                     "coffee.png", "confused.png", "cool.png", "cry.png", "dont_know.png","eat.png", "erm.png", 
                     "girl.png", "joking.png", "kiss.png", "kissed.png", "lol.png", "love.png", 
                     "money.png", "nerd.png", "party.png", "rose.png", 
                     "rose_wilted.png", "sad.png", "scared.png", "sick.png", "silent.png", "sleep.png", 
                     "smile.png", "stop.png", "surprized.png", "thinking.png", "thumb.png", "thumb_down.png", 
                     "tongue.png", "vicious.png", "wine.png", "wink.png"                
                    ];

    var HTMLStr = "";

    for(var i = 0; i < emoticons.length; i++){

        HTMLStr += "<img class='addEmoticon' src='emoticons/" + emoticons[i] + "' />";

    }

    $("#showEmoticons").html(HTMLStr).fadeIn("fast", function(){
        var emoticonHeight = Number($(".conversation_footer").height()) + 52 
        var max_height = $(document).height() - emoticonHeight;
        $(".conversation_body").css({ "height" : max_height });
    });



})

//CLose emoticons
$(document).on("click", ".conversation_body", function(){

    var max_height = $(document).height() - 92;
    $("#showEmoticons").fadeOut("fast");
    $(".conversation_body").css({ "height" : max_height });


})


$(document).on("focus", ".chat-text-box", function(){

    var max_height = $(document).height() - 92;
    $("#showEmoticons").fadeOut("fast")
    $(".conversation_body").css({ "height" : max_height });


})

$(document).on("blur", ".chat-text-box", function(){
    $(document).click();
})


//View profile picture
$(document).on("click", "#view_profile_picture", function(){

    var user = $("#view_profile_username").text();
    var img = $(this).children("img").attr("src")

    $("#profilePictureView").fadeIn('slow', function(){
        $("#profilePictureViewHeader").text(user);
        $("#profilePictureViewBody").html($("<img/>").attr("src", img));

    })

    

})


//GO back from viewing profile picture
$(document).on("click", "#profilePictureViewHeader", function(){
    $("#profilePictureView").fadeOut('slow', function(){
        $("#profilePictureViewHeader").empty();
        $("#profilePictureViewBody").empty();
    })
})


//View profile from favorites
$(document).on("click", "#view_profile", function(){

    var _this = $(this);

    $("#view-user-details").load("pages/profile.html");
    
    $("#tapHoldMenu").hide();

    $("#view-user-details").fadeIn("fast", function(){

        var user = _this.attr("data-user");
        var profile = _this.attr("data-profile");
        var aboutMe = _this.attr("data-about");
        var limit = 200;

        storage.set('menu', "profile");

        $("#dislike-button, #like-button").hide();
        $("#view_profile_username").text(user);
        $("#view_profile_picture img").attr("src", profile);
        $("#view_profile_gender").text("---");

        var total = aboutMe.length;

        var btnReadMore = "<span class='btnReadMore'> ...View All</span>";

        if(total > limit){
            aboutMe = aboutMe.substr(0, limit);
            $("#view_profile_about").html("<b>About " + user + ":</b> " + aboutMe + btnReadMore);
        }else{
            $("#view_profile_about").html("<b>About " + user + ":</b> " + aboutMe);
        }


        $(document).on("click", ".btnReadMore", function(){
            $("#view_profile_about").html("<b>About " + user + ":</b> " + aboutMe);
        })

    });

})



//Go to another tab in edit match maker
$(document).on("click", ".stagesCover", function(){

   var id = $(this).attr("data-content");
   var _this = $(this);

    $(".stagesCover").not(this).removeClass("stageSelected"); //Remove class

   $(".editStageCover").not("#" + id).fadeOut("fast", function(){
        _this.addClass("stageSelected"); //Add class to selected tab
        $("#" + id).fadeIn("slow"); //Show selected field
        $("#matchMakerProfileCover").animate({ "scrollTop" : 0 }, 350);
    })

})


//Edit fields by clicking pencil
$(document).on("click", '.editInputFields', function(){
    $(this).hide().prev(".textInpuEditor")
                  .css({ "width" : "100%" })
                  .attr("disabled", false).focus();
})


//Restore edit pencil
$(document).on("blur", ".textInpuEditor", function(){
    $(this).css({ "width" : "85%" }).attr("disabled", true)
           .next('.editInputFields').show()
})


//Textarea fit text
$(document).on("keyup", ".textInpuEditor", function(){

    if (!$(this).prop('scrollTop')) {
        do {
            var b = $(this).prop('scrollHeight');
            var h = $(this).height();
            $(this).height(h - 5);
        }

        while (b && (b != $(this).prop('scrollHeight')));
    };

    $(this).height($(this).prop('scrollHeight'));

})


$(document).on("click", "#selDateCover, #viewEditPictureOptions", function(e){
    e.stopPropagation();
})

//Insert data in a calendar
$(document).on("click", ".selectDate", function(e){

    var _this = $(this).attr("id");
    var nowDate = new Date();
    var defaultDay = nowDate.getDate();
    defaultDay = (defaultDay < 10) ? "0" + defaultDay : defaultDay;
    var defaultMonth = nowDate.getMonth();
    var actualMonthNumber = defaultMonth + 1;
    var actualMonth = (actualMonthNumber < 10) ? "0" + actualMonthNumber : actualMonthNumber;
    var defaultYear = nowDate.getFullYear() - 18;
    var actualDate =  defaultDay  + " " + actualMonth + " " + defaultYear

    $("#selDateCover").fadeIn("slow", function(){
        $("#selDateAccept").attr({"field" : _this, "data-date" : actualDate });
        $("#selDateHeader").text(defaultDay + " " + getMonthInWords(defaultMonth) + " " +  defaultYear);
        $("#selDateDay").text(defaultDay);
        $("#selDateMonth").attr("data-value", defaultMonth).text(getMonthInWords(defaultMonth));
        $("#selDateYear").text(defaultYear);
    });

    e.stopPropagation();
})



$(document).click(function(){
     $("#selDateCover").fadeOut("slow");
     $("#viewEditPictureOptions").fadeOut("slow", function(){
        $(this).hide();
     })
})


//Get months in wording
function getMonthInWords(mon){

    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    return months[mon];

}


//Change day up
$(document).on("click", "#dayMoveUp", function(){

    var day = Number($("#selDateDay").text()) + 1;
    var day = (day < 10) ? "0" + day : day;
    var month = $("#selDateMonth").text();
    var year = $("#selDateYear").text();
    var actualDate;

    if(day == 32){
        actualDate =  "01 " + month + " " + year;
        $("#selDateDay").text("01");
    }else{
        actualDate = day + " " + month + " " + year;
        $("#selDateDay").text(day);
    }

    $("#selDateHeader").text(actualDate);

})



//Move down below
$(document).on("click", "#dayMoveDown", function(){

    var day = Number($("#selDateDay").text()) - 1;
    var day = (day < 10) ? "0" + day : day;
    var month = $("#selDateMonth").text();
    var year = $("#selDateYear").text();
    var actualDate;
    
    if(day == 00){
        actualDate = 31 + " " + month + " " + year;
        $("#selDateDay").text(31);
    }else{
        actualDate = day + " " + month + " " + year;
        $("#selDateDay").text(day);
    }

    $("#selDateHeader").text(actualDate);


})



//Change month up
$(document).on("click", "#monthMoveUp", function(){


    var day = $("#selDateDay").text();
    var month = $("#selDateMonth").text();
    var monthInt = Number($("#selDateMonth").attr("data-value")) + 1;
    var year = $("#selDateYear").text();
    var actualDate;

    if(monthInt == 12){
        $("#selDateMonth").attr("data-value", 0).text(getMonthInWords(0))
        actualDate = day + " " + getMonthInWords(0) + " " + year;
    }else{
        $("#selDateMonth").attr("data-value", monthInt).text(getMonthInWords(monthInt))
        actualDate = day + " " + getMonthInWords(monthInt) + " " + year;
    }

    $("#selDateHeader").text(actualDate);    

})


//Change month down
$(document).on("click", "#monthMoveDown", function(){


    var day = $("#selDateDay").text();
    var month = $("#selDateMonth").text();
    var monthInt = Number($("#selDateMonth").attr("data-value")) - 1;
    var year = $("#selDateYear").text();
    var actualDate;

    if(monthInt == -1){
        $("#selDateMonth").attr("data-value", 11).text(getMonthInWords(11))
        actualDate = day + " " + getMonthInWords(11) + " " + year;
    }else{
        $("#selDateMonth").attr("data-value", monthInt).text(getMonthInWords(monthInt))
        actualDate = day + " " + getMonthInWords(monthInt) + " " + year;
    }

    $("#selDateHeader").text(actualDate);    

})




//Change year up
$(document).on("click", "#yearMoveUp", function(){

    var day = $("#selDateDay").text();
    var month = $("#selDateMonth").text();
    var year = Number($("#selDateYear").text()) + 1;
    var max = Number(new Date().getFullYear()) + 70;
    var min = new Date().getFullYear() - 70;
    var actualDate;

    if(year == max){
        $("#selDateYear").text(min);
        actualDate = day + " " + month + " " + year;
    }else{

        $("#selDateYear").text(year);
        actualDate = day + " " + month + " " + year;
    }

    $("#selDateHeader").text(actualDate);

})



//Move year below
$(document).on("click", "#yearMoveDown", function(){

    var day = $("#selDateDay").text();
    var month = $("#selDateMonth").text();
    var year = Number($("#selDateYear").text()) - 1;
    var max = Number(new Date().getFullYear()) + 70;
    var min = new Date().getFullYear() - 70;
    var actualDate;

    if(year == min){
        $("#selDateYear").text(max);
        actualDate = day + " " + month + " " + year;
    }else{

        $("#selDateYear").text(year);
        actualDate = day + " " + month + " " + year;
    }

    $("#selDateHeader").text(actualDate);


})


$(document).on("click", "#selDateCancel", function(){

    $("#selDateCover").fadeOut("slow");

})



$(document).on("click", "#selDateAccept", function(){

    var field = $(this).attr("field");
    var year = $("#selDateYear").text();
    var month = Number($("#selDateMonth").attr("data-value")) + 1;
    var month = (month < 10) ? "0" + month : month;
    var day = $("#selDateDay").text();
    var formatedDate = year + "/" + month + "/" + day;
     
    $("#selDateCover").fadeOut("slow", function(){
        $("#" + field).val(formatedDate);
        $("#" + field).parent().siblings('.selectYourInfoHeader').text("Date of birth: " + formatedDate)

    });

})


//Change about me
$(document).on("change", "#input_aboutme", function(){

    $("#infoAlertNotifcation").text("Changes has been saved!").fadeIn("slow").delay(2000).fadeOut("fast");

})


//Select my gender
$(document).on("click", ".selectCheckBox", function(){
    
    var selectedOpt = $(this).attr("data-value"); 
    var key = $(this).attr("data-obj"); 

    $(this).parent(".selectInfoCover").parent(".selectYourInfoBody")
           .siblings(".selectYourInfoHeader")
           .children(".OptSelectedLabel")
           .text(selectedOpt);
    
    matchMaker[key] = selectedOpt;

    storage.set("matchMaker", matchMaker);

    $("#infoAlertNotifcation").text("Changes has been saved!").fadeIn("slow").delay(2000).fadeOut("fast");

})
    
//Check box 
$(document).on("click", ".selectRealCheckBox", function(){
    
    var selectedOpt = "";

    if($("#genderLookingMale").is(":checked") && $("#genderLookingFemale").is(":checked")){
        selectedOpt = "Both";
    }
    else if($("#genderLookingMale").is(":checked")){
        selectedOpt = "Male";
    }
    else if($("#genderLookingFemale").is(":checked")){
        selectedOpt = "Female";
    }else{
        $("#genderLookingFemale").prop("checked", true);
        selectedOpt = "Female";
    }

    $(this).parent(".selectInfoCover").parent(".selectYourInfoBody")
           .siblings(".selectYourInfoHeader")
           .children(".OptSelectedLabel")
           .text(selectedOpt);
  
    $("#infoAlertNotifcation").text("Changes has been saved!").fadeIn("slow").delay(1000).fadeOut("fast");

})


$(document).on("focus", "#myLanguages", function(){

    $("#langaugesCover").fadeIn("fast", function(){

        var docHeight = $(document).height() - 40;

        $("#languageBody").css({"max-height" : docHeight + "px"});

    })

})


$(document).on("keyup", "#NumberOfChilds", function(){

    var entry = $(this).val();
    var numberInWords = ["Zero", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen"];
    var inWords = numberInWords[entry];

    if(numberInWords.indexOf(inWords) === -1){
        inWords = "None";
    }
    
    $("#NumberOfChilds").parent(".selectYourInfoBody")
                     .siblings(".selectYourInfoHeader")
                     .children(".OptSelectedLabel")
                     .text(inWords);    

})


$(".langButton").each(function(){

    $(this).on("click", function(){

        var lang =  $(this).text();

        $("#langaugesCover").fadeOut("fast", function(){

            $("#myLanguages").val(lang).parent(".selectYourInfoBody")
                             .siblings(".selectYourInfoHeader")
                             .children(".OptSelectedLabel")
                             .text(lang);

        })

    })

})


})
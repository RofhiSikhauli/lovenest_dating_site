//Storage object
var storage = {
 
    set : function(name, value){
        window.localStorage.setItem(name, JSON.stringify(value)); 
    },
    get : function(name){
        return JSON.parse(window.localStorage.getItem(name));
    },
    remove : function(name){
        window.localStorage.removeItem(name);
    }
}
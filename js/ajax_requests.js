$(document).ready(function(){


//Storage
var storage = {
 
    set : function(name, value){
        window.localStorage.setItem(name, JSON.stringify(value)); 
    },
    get : function(name){
        return JSON.parse(window.localStorage.getItem(name));
    },
    remove : function(name){
        window.localStorage.removeItem(name);
    }
}


//Loadings 
setTimeout(load_chats, 100)   
setTimeout(load_suggestions, 3000);


//Check the is a previous app login
if(storage.get(username) === null){
    $("#register_button").show();
}


//Set variables using elements
var login_input = ".login-input";
var content = "#content";
var link_button = ".link_button";
var frm_login = "#formLogin";
var login_button = "#loginButton";
var login_id = "#loginID";
var login_pass = "#loginPass";
var page = "#page";
var tabs = ".header-menu";
var search_box = "#input-search-box";
var search_cover = "#search-users-border";
var online_user = "#online-users-cover";
var dimmer = "#screen_dimmer";
var loader = "#request_loader";
var load_text = ".displayFormErrors";
var frmReg = "#frmRegister";
var username = "#username";
var email = "#email";
var userPass = "#userPass";
var confirmPass = "#confirmPass";
var regBtn = "#submitRegButton";
var search_button = "#top-search";
var search_box = "#search_text";
var open_setting = "#top-settings";

//Manage back button
document.addEventListener("deviceready", onDeviceReady, false);

function onDeviceReady() {
    document.addEventListener("backbutton", backKeyDown, true);
}
    
function backKeyDown() {   

    var menu = storage.get("menu") || "home";

    if(menu === "home"){
        navigator.app.exitApp();
    } 

    if(menu === "favorites"){
        $("#chat").trigger("click");
        storage.set("menu", "home");
    } 

    if(menu === "matches"){
        $("#chat").trigger("click");
        storage.set("menu", "home");
    }

    if(menu === "profile"){
        $("#go_back_hooks").trigger("click");
        storage.set("menu", "matches");
    } 

    if(menu === "conversation"){

        $(".user_name_view").trigger("click");
        storage.set("menu", "home");
        
        if($("#display_favorites").length){
            storage.set("menu", "favorites");
        }
  
    } 


    if(menu === "options"){
        $(document).click();

        storage.set("menu", "home");

        if($("#display_favorites").length){
            storage.set("menu", "favorites");
        }

        if($("#hookups").length){
            storage.set("menu", "matches");
        }   

    } 


    if(menu === "popMenu"){
        $("#tapHoldMenu").trigger("click");
    }

    if(menu === "optionsMenu"){
        $("#menu_pop_title").trigger("click");
            storage.set("menu", "options");
    }

}

var isLogin = storage.get('isLogin');
var action = "";
var frm_data = {}

//Login in user after app auto logging
if(isLogin === true){
    
   $.ajax({
        url : action + "?callback=register",
        dataType : 'jsonp',
        data : frm_data,
        success : function(data){
            
            //alert(data.status)
            if(data.status != "pass"){
 


            }else if(data.status === "pass"){

            }
        } 
    })
}


//Check existing users
$(document).on("blur", username + ", " + email, function(){

    var _this = $(this);
    var text = _this.val();
    var action = $(frmReg).attr("action");
    var frm_data = { "field" : text };
    var fieldName = _this.attr("id");

    if(_this.val() === "") return;

    $.ajax({
        url : action + "?callback=Register&action=check",
        dataType : 'jsonp',
        data : frm_data,
        success : function(data){
            if(data.status === 1){
                $('input').attr("disabled", true);
                $(".displayFormErrors").html(fieldName.charAt(0).toUpperCase() + fieldName.slice(1) + " not available")
                                       .fadeIn("slow")
                                       .delay(3000)
                                       .fadeOut("slow", function(){

                                            _this.val("").focus();
                                            $('input').attr("disabled", false);

                                       });

                return;

            }
        }
    })

})


//Submit signing up user's form
$(document).on("click", regBtn, function(){
    
    var frm_data = $(frmReg).serialize();
    var action = $(frmReg).attr("action");
    var user = $(username).val();
    var user_email = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*.(\.\w{2,3})+$/.test($(email).val());
    var regPass = $(userPass).val();
    var regConfirmPass = $(confirmPass).val();

    if(user === ""){
        $(".displayFormErrors").html("Username required")
                               .fadeIn("slow")
                               .delay(3000)
                               .fadeOut("slow");
        $(username).focus();
        return;
    } 

    if($(email).val() === ""){
        $(".displayFormErrors").html("Email required")
                               .fadeIn("slow")
                               .delay(3000)
                               .fadeOut("slow");
        $(email).focus();
        return;       
    }  

    if(user_email !== true){
        $(".displayFormErrors").html("Invalid email address")
                               .fadeIn("slow")
                               .delay(3000)
                               .fadeOut("slow");
        $(email).focus();
        return;       
    }  

    if(regPass === ""){
        $(".displayFormErrors").html("Please enter password")
                               .fadeIn("slow")
                               .delay(3000)
                               .fadeOut("slow");
        $(regPass).focus();
        return;       
    }  

    if(regPass.length < 5){
        $(".displayFormErrors").html("Password too short")
                               .fadeIn("slow")
                               .delay(3000)
                               .fadeOut("slow");
        $(regPass).focus();
        return;       
    } 

    if(regConfirmPass !== regPass){
        $(".displayFormErrors").html("Password mismatch")
                               .fadeIn("slow")
                               .delay(3000)
                               .fadeOut("slow");

        $(regConfirmPass).focus();
        return;         
    }

    //Validation has passed submit form

    jSpinner.startSpinner("Signing up...");

    $.ajax({
        url : action + "?callback=Register&action=reg",
        dataType : 'jsonp',
        data : frm_data,
        success : function(data){
            
            if(data.status === "fail"){
                jSpinner.stopSpinner();
                $(".displayFormErrors").html("Oops! An error occurred")
                                       .fadeIn("slow")
                                       .delay(3000)
                                       .fadeOut("slow");

            }else if(data.status === "pass"){
                jSpinner.stopSpinner();
                window.location = "home.html";

            }else{
                jSpinner.stopSpinner();
                $(".displayFormErrors").html(data.status)
                                       .fadeIn("slow")
                                       .delay(3000)
                                       .fadeOut("slow");                
            }

        },
        error : function(error){
            jSpinner.stopSpinner();
            $(".displayFormErrors").html("Oops! An error occurred")
                                   .fadeIn("slow")
                                   .delay(3000)
                                   .fadeOut("slow");

        }

    })


})


//Logging in
$(document).on("click", login_button, function(){

    var frm_data = $(frm_login).serialize();
    var action = $(frm_login).attr("action");

    if($(login_id).val() != "" && $(login_pass).val() != ""){
        jSpinner.startSpinner("signing in...");

        $.ajax({
            url : action,
            dataType : 'jsonp',
            data : frm_data,
            success : function(data){
                
                if(data.status === "fail"){  

                    jSpinner.stopSpinner();
                    $(".displayFormErrors").html("Wrong login credintials")
                                           .fadeIn("slow")
                                           .delay(3000)
                                           .fadeOut("slow");

                }else if(data.status === "pass"){
                        jSpinner.stopSpinner();                      
                        storage.set('isLogin', true);
                        storage.set('username', data.username);
                        storage.set('profile', data.profile);
                        storage.set('email', data.email);
                        window.location = "home.html";

                } 
            },
            error : function(data){
                jSpinner.stopSpinner();
                $(".displayFormErrors").html("Oops! An error occurred")
                                       .fadeIn("slow")
                                       .delay(3000)
                                       .fadeOut("slow");
            }
            
        })


    }


})


//load previous chats
function load_chats(){

    var tbl_chats =  [];
    var tbl_fav = storage.get("favorites") || [];
    var count = 0;

    if(tbl_fav.length > 0){

        for(var i = 0; i < tbl_fav.length; i++){   

            var user_msg = tbl_fav[i].username.replace(/\s+/g, '');
            var table_mess = storage.get(user_msg) || [];
            
            if(table_mess.length > 0){

                 var obj = {};

                if("seen" in table_mess[0]){
                   
                    obj.username = tbl_fav[i].username,
                    obj.profile = tbl_fav[i].profile,
                    obj.msg = table_mess[0].message,
                    obj.seen = table_mess[0].seen,
                    obj.date = table_mess[0].send_date,
                    obj.time = table_mess[0].send_time
                    obj.status = tbl_fav[i].status                    

                }else if("read" in table_mess[0]){
                    
                    obj.username = tbl_fav[i].username,
                    obj.profile = tbl_fav[i].profile,
                    obj.msg = table_mess[0].message,
                    obj.read = table_mess[0].read,
                    obj.date = table_mess[0].send_date,
                    obj.time = table_mess[0].send_time
                    obj.status = tbl_fav[i].status
                                       
                }

                tbl_chats.push(obj);

            }

            //Count number of unread messages
            for(c in table_mess){

                if("read" in table_mess[c]){
                    if(table_mess[c].read == 0){
                        count++;
                    }
                }
            }

        }

        var chats = $.grep(tbl_chats, function(n){ return(n) });
        storage.set("chats", chats); //Save chats
    
    }

    //alert(count)

    if(count > 2){
        $("#unreadChatCount").text("2+");
        $("#unreadChatCount").show();
    }else if(count !== 0){
        $("#unreadChatCount").text(count);
        $("#unreadChatCount").show();
    }else if(count === 0){
        $("#unreadChatCount").hide();
    }
      
    display_chats(); //Display chats           

}


//Get suggested users
function load_suggestions(){
        
    var action = "http://www.lovenest.co.za/App_Models/GetSuggestions.php?callback=Suggestions";
    var frm_data = { 'username' : storage.get('username') };

    $.ajax({
        url : action,
        dataType : 'jsonp',
        data : frm_data,
        async : true,
        success : function(data){

            if(data.length != 0){

                var arr_sugg = [];

                for(var i in data){    
                    var obj = {
                        'username' : data[i].username,
                        'gender' : data[i].gender,
                        'age' : data[i].age,
                        'about' : data[i].about,
                        'profile' : data[i].profile
                    }
                     
                    arr_sugg[i] = obj;

                }

                storage.set("suggestions", arr_sugg); //Save suggestions before displaying   
                
            }
        }
    });

    
    setTimeout(load_suggestions, 600000);

}


//Calculate days between two dates
function calculateDays(nowDate){

    var arr = nowDate.split("/");
    var currDate = new Date();
    var currYear = currDate.getFullYear();
    var currMon = Number(currDate.getMonth() + 1);
    var currDay = currDate.getDate();
    
    var day = arr[2];
    var mon = arr[1];
    var year = arr[0];

    var oneDay = 24*60*60*1000;
    var firstDate = new Date(year, mon, day);
    var secondDate = new Date(currYear, currMon, currDay);

    var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));

    return diffDays;

}


//Get and display online users
function display_chats(){

    var tbl_chats = storage.get("chats") || [];
    var HTMLStr = "";

    if(tbl_chats.length === 0){
        HTMLStr += "<em>No previous chats<br /><br />Go to favorites and start conversation</em>";

    }else{
        for(var i in tbl_chats){
            
            if("read" in tbl_chats[i]){
                
                var WhenTime =  tbl_chats[i].time;

                if(calculateDays(tbl_chats[i].date) === 1){
                    WhenTime = "Yesterday";
                }else if(calculateDays(tbl_chats[i].date) > 1){
                    WhenTime = tbl_chats[i].date ;
                }

                var statusTime = (tbl_chats[i].read === 0) ? "<span style='color: #F00'>Unread</span>" : WhenTime;
                var onlineStatus = (tbl_chats[i].status === "") ? "<div class='onlineIcons'></div>" : "";

                HTMLStr += "<div class='online-users-cover'><div class='chat_user_pic'>" +
                             "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_chats[i].profile + "' />"+
                             "</div>" + 
                             "<div class='chat_username'>" + onlineStatus + tbl_chats[i].username +  "</div>"+
                             "<div class='unread'>" + statusTime +  "</div>"+
                             "<div class='review_message incomings'>" +  tbl_chats[i].msg + "</div>"+ 
                             "</div>";           
            }else{
                var sent = (tbl_chats[i].seen === 0) ? "<img src='img/notSent.png' style='width: 12px' />" : "<img src='img/tick.png' style='width: 12px' />";
                var onlineStatus = (tbl_chats[i].status === "") ? "<div class='onlineIcons'></div>" : "";
                
                var WhenTime =  tbl_chats[i].time;

                if(calculateDays(tbl_chats[i].date) === 1){
                    WhenTime = "Yesterday";
                }else if(calculateDays(tbl_chats[i].date) > 1){
                    WhenTime = tbl_chats[i].date;
                }

                HTMLStr += "<div class='online-users-cover'><div class='chat_user_pic'>" +
                             "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_chats[i].profile + "' />"+                            
                             "</div>" + 
                             "<div class='chat_username'>" + onlineStatus + tbl_chats[i].username +  "</div>"+
                             "<div class='unread'>" + WhenTime +  "</div>"+ 
                             "<div class='review_message'>" +  sent + " <span class='chatsMessageReview'>" + tbl_chats[i].msg + "</span></div>"+ 
                             "</div>"; 


            }

        }
    }


    if($("#display_chats").length){

        $("#display_chats").html(HTMLStr);

    }
        

}



$(document).on("click", "#like-button", function(){
    
    var user = storage.get('user_invite');
    var profile = storage.get('profile_invite');
    var about = (storage.get('about') === "") ? "not available" : storage.get('about');
    var table = storage.get('favorites') || [];

    var obj = { "username" :  user, "profile" : profile, 'about' : about, 'status' : "" }

    table.unshift(obj);
    storage.set('favorites', table);  

    $("#like-button").hide();
    $("#dislike-button").hide();
    $("#likedOrDislike").fadeIn("fast", function(){
        $(this).text(user + " added");
        storage.remove('user_invite');
        storage.remove('profile_invite');
        var tbl_sugg = storage.get("suggestions") || [];
        var tbl_fav = storage.get("favorites") || [];
        var table_dislike = storage.get('dislikes') || [];
        var max_height = $(document).height() - 70;
        var HTMLStr = "";

        $("#hookups").css({ "max-height" : max_height, "overflow-y" : "auto" });
       
        //Display first suggest
        if(tbl_sugg.length > 0){

            loopSugg: for(var i in tbl_sugg){
                
                if(table_dislike.indexOf(tbl_sugg[i].username) !== -1) {
                    tbl_sugg.splice(i, 1);
                    continue;
                }

                for(m in tbl_fav){
                    if(tbl_sugg[i].username == tbl_fav[m].username){
                        continue loopSugg;                           
                    }
                }

                var gender = (tbl_sugg[i].gender !== "") ? tbl_sugg[i].gender : "";
                var profile = (tbl_sugg[i].profile !== "") ? tbl_sugg[i].profile : "";
                var age = (tbl_sugg[i].age !== "") ? ", " + tbl_sugg[i].age + " years" : "";
                var about = (tbl_sugg[i].about === "") ? "not available" : tbl_sugg[i].about;

                HTMLStr += "<div class='matches_cover' data-about='" + about + "' data-gender='" + gender + age + " ' data-profile='" + tbl_sugg[i].profile + "' data-user='" + tbl_sugg[i].username + "'>"+
                         "<div class='chat_user_pic'>" +
                         "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_sugg[i].profile + "' />"+
                         "</div>" + 
                         "<div class='chat_username'>" + tbl_sugg[i].username +  "</div>"+
                         "<div class='hook_gender_age'>" +  gender + age + "</div>"+ 
                         "</div>";  
                                               

            }

            $("#loadHooker").hide();    
            $("#hookups").append(HTMLStr);            
        }else{

            $("#loadHooker").hide();
            HTMLStr += "<em>No suggestions found. <br /><br />Please ensure that you filled your match marker form</em>";
            $("#hookups").append(HTMLStr);
        }




    });


})

 // storage.remove('dislikes')
$(document).on("click", "#dislike-button", function(){

    var table_dislike = storage.get('dislikes') || [];
    var user = storage.get('user_invite');
    var confirmDislike = confirm(user + " will be removed from your matches. \n\nContinue?");

    if(!confirmDislike) return;

    table_dislike.push(user);
    storage.set('dislikes', table_dislike);

    $("#go_back_hooks").trigger("click");

    var tbl_sugg = storage.get("suggestions") || [];
    var tbl_fav = storage.get("favorites") || [];
    var table_dislike = storage.get('dislikes') || [];
    var max_height = $(document).height() - 70;
    $("#hookups").css({ "max-height" : max_height, "overflow-y" : "auto" });
    var HTMLStr = "";

    //Display first suggest
    if(tbl_sugg.length > 0){

        loopSugg: for(var i in tbl_sugg){
            
            if(table_dislike.indexOf(tbl_sugg[i].username) !== -1) {
                tbl_sugg.splice(i, 1);
                continue;
            }

            for(m in tbl_fav){
                if(tbl_sugg[i].username == tbl_fav[m].username){
                    continue loopSugg;                           
                }
            }

            var gender = (tbl_sugg[i].gender !== "") ? tbl_sugg[i].gender : "";
            var profile = (tbl_sugg[i].profile !== "") ? tbl_sugg[i].profile : "";
            var age = (tbl_sugg[i].age !== "") ? ", " + tbl_sugg[i].age + " years" : "";
            var about = (tbl_sugg[i].about === "") ? "not available" : tbl_sugg[i].about;

            HTMLStr += "<div class='matches_cover' data-about='" + about + "' data-gender='" + gender + age + " ' data-profile='" + tbl_sugg[i].profile + "' data-user='" + tbl_sugg[i].username + "'>"+
                     "<div class='chat_user_pic'>" +
                     "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_sugg[i].profile + "' />"+
                     "</div>" + 
                     "<div class='chat_username'>" + tbl_sugg[i].username +  "</div>"+
                     "<div class='hook_gender_age'>" +  gender + age + "</div>"+ 
                     "</div>";  
                                           

        }

        $("#loadHooker").hide();    
        $("#hookups").append(HTMLStr);            
    }else{

        $("#loadHooker").hide();
        HTMLStr += "<em>No suggestions found. <br /><br />Please ensure that you filled your match marker form</em>";
        $("#hookups").append(HTMLStr);
    }


})
    
//Get and display favorites users
function display_favorites(){

    var tbl_name = storage.get("favorites") || [];
    var HTMLStr = "";
    //alert(tbl_name.length);

    if(tbl_name.length === 0){
        HTMLStr += "<em>No favorites<br /><br />Go to matches tab and view your matches</em>";
    }else{
        for(var i in tbl_name){

            var about = (tbl_name[i].about === "") ? "not available" : tbl_name[i].about;
            var onlineStatus = (tbl_name[i].status === "") ? "<div class='onlineIcons'></div>" : "";

            HTMLStr += "<div class='fav-users-cover' data-user='"  + tbl_name[i].username +  "'><div class='chat_user_pic'>" +
                       "<img src='http://www.lovenest.co.za/profile_pics/" + tbl_name[i].profile + "' />"+
                       "</div>" + 
                       "<div class='chat_username'>" + onlineStatus + tbl_name[i].username + "</div>"+ 
                       "<div class='user-fav-review'>" + tbl_name[i].about + "</div>"+ 
                       "</div>";
           

        }
    }

    return HTMLStr;

}


//Display favorites
setInterval(function(){

    $("#display_favorites").html(display_favorites);
    load_chats();

}, 300);


//Get Profile Pictures
function getFavoritesInfo(){

    var tbl_fav = storage.get('favorites') || [];
    var action = "http://www.lovenest.co.za/App_Models/getFavoritesInfo.php?callback=GetProfile"

    if(tbl_fav.length === 0) return;

    for(var i = 0; i < tbl_fav.length; i++){
        var user = tbl_fav[i].username;
        var data = {"id" : i, "username" : user};

        $.ajax({
            url : action,
            dataType : 'jsonp',
            data : data,
            async : true,
            success : function(data){

                if(data.status === true){
                    var about = (data.about === "") ? "not available" : data.about;
                    tbl_fav[data.id].profile = data.profile;
                    tbl_fav[data.id].about = about;
                    storage.set('favorites', tbl_fav);                                          
                }
            }
        })

    }

}

setInterval(getFavoritesInfo, 120000); //Get user info from server after 2 minutes




//Search users
$(document).on("keyup", "#user_search", function(){


    var action = "http://www.lovenest.co.za/App_Models/Search_Users.php?callback=SearchUsers";
    var query = $(this).val();
    var params = {'q' : query};

    if($(this).val() != "" && $("#searchresultsDisplay").html() == ""){
        $("#searchresultsDisplay").html("<div id='searchSpinner'>Searching...</div>");
    }else{
        $("#searchSpinner").remove();
    }
     

    $.ajax({
        url : action,
        dataType : 'jsonp',
        data : params,
        success : function(res){

            var HTMLStr = "";

            if(res.length > 0){

                for(var i in res){

                    var gender = (res[i].gender !== "") ? res[i].gender : "";
                    var profile = (res[i].profile !== "") ? res[i].profile : "";
                    var age = (res[i].age !== "") ? ", " + res[i].age + " years" : "";
                    var about = (res[i].about === "") ? "not available" : res[i].about;
                    var keyWord = "<span class='searchKeyword'>" + query + "</span>";
                    var regexp = new RegExp(query, "gi");
                    var username = res[i].username.replace(regexp, keyWord);
                    
                    HTMLStr += "<div class='matches_cover' data-about='" + about + "' data-gender='" + gender + age + " ' data-profile='" + res[i].profile + "' data-user='" + res[i].username + "'>"+
                               "<div class='chat_user_pic'>" +
                               "<img src='http://www.lovenest.co.za/profile_pics/" + res[i].profile + "' />"+
                               "</div>" + 
                               "<div class='chat_username'>" + username +  "</div>"+
                               "<div class='hook_gender_age'>" +  gender + age + "</div>"+ 
                               "</div>"; 
                }                
            }

            // jSpinner.stopSpinner();

            $("#searchresultsDisplay").html(HTMLStr)
                                      .append($("<div id='searchQuery'/>")
                                      .html(res.length + " result(s) for <b>\"" + query + "\"</b>"));
            
        }
    })


})



// storage.format()

//End of file
})
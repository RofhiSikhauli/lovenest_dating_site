//This file store messages in a localstorage
//New incoming messages will be first stored in a localstorage before displayed
//New outgoing message will be sent to the server at the same time it will be saved in localstorage also

//document.addEventListener('deviceready', function(){
$(document).ready(function(){	

	//Format time to am/pm
	var format_time = function(date) {
		  var hours = date.getHours();
		  var minutes = date.getMinutes();
		  var ampm = hours >= 12 ? 'pm' : 'am';
		  hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0' + minutes : minutes;
		  var strTime = hours + ':' + minutes + ampm;
		  return strTime;
		}

	var server_request = function(action, param){
		
	    $.ajax({
	        url : action,
	        dataType : 'jsonp',
	        data : param,
	        async : true,
	        success : function(data){
	        	
 				if(data.action === "send"){
 					
 					if(data.status === 1){

 						var user = data.receiver.replace(/\s+/g, '');
 						var table_name = storage.get(user) || [];
 						var code = data.code;
 						
 						for(var i in table_name){
 							if(table_name[i].code == code){
 								table_name[i].seen = 1;
 								storage.set(data.receiver, table_name);
 								var new_table = storage.get(data.receiver);
 								var HTMLStr = "";
 								var max = $(document).height() - 100;

 								for(var key in new_table){

						            if("seen" in new_table[key]){
						                var sent = (new_table[key].seen == 0) ? "<img src='img/notSent.png' style='width: 12px' />" : "<img src='img/tick.png' style='width: 12px' />";
						                HTMLStr += "<div class='conversation_outgoing'>"+
						                     "<div class='outgoing_conversation_cover'>" + new_table[key].message + "</div>" +
						                     "<div class='send-time_send-status'>" + new_table[key].send_time + " " + sent + " </div>" +
						                     "<br style='clear : both' /></div>";            
						                
						            }else{
						                new_table[key].read = 1;
						                storage.set(data.receiver, new_table);
                
						                HTMLStr += "<div class='conversation_income'>"+
						                     "<div class='income_conversation_cover'>" + new_table[key].message + "</div>" +
						                     "<div class='receive-time'>" + new_table[key].send_time + "</div>" + 
						                     "<br style='clear : both' /></div>";

						            }
				                }

				                $(".conversation_body").css({'height' : max, 'overflow-y' : 'auto'}).html(HTMLStr);

 								break;
 							}
 								
 						}

						AutoSendMessage(action);

 					}

 				}else{
 					//Receive incoming messages
			    	 messaging.incoming(data);		    	

 				}

	        }
	    });		
	}	

	//Messages object
	var messaging = {

		incoming : function(msg_obj){
			// alert(msg_obj.toSource())
		 	//Initialize an anrray
			var data = msg_obj;
			var currentdate = new Date();
			var month = Number(currentdate.getMonth()) + 1;
			var month = (month < 10) ? '0' + month : month;
			var day = currentdate.getDate();
			var day = (day < 10) ? '0' + day : day;
			var curr_date = currentdate.getFullYear() + '/' + month + '/' + day;
			var url = "http://www.lovenest.co.za/App_Models/Messages.php?callback=callback";
			var params = { 'action' : 'receive', 'receiver' : storage.get('username') };
			var HTMLStr = "";
			var max = $(document).height() - 170;
			var tbl_fav = storage.get("favorites") || [];

			//No new message returned, resend another request
			if(data.length === 0){
				server_request(url, params);
				return;
			}

			BeepNow(); //Beep once
			
			var count = Number($("#unreadChatCount").text()) + Number(data.length);
			
			if(count > 2){
				$("#unreadChatCount").text("2+");
				$("#unreadChatCount").show();
			}else if(count !== 0){
				$("#unreadChatCount").text(count);
				$("#unreadChatCount").show();
			}else if(count === 0){
				$("#unreadChatCount").hide();
			}


			for (var i = 0; i < data.length; i++) { 

				var obj = { 
					'sender' : data[i].sender,
					'message' : data[i].message,
					'send_time' : format_time(currentdate),
					'send_date' : curr_date,
					'read' : 0
					}

				var table_name = obj.sender.replace(/\s+/g, ''); //remove spaces by underscore
				var table = storage.get(obj.sender) || [];  //Tables name
				var counter = 0;

				table.unshift(obj);
				storage.set(table_name, table);	

				//Check if sender is already in favorites table and move to the top
				if(tbl_fav.length !== 0){

					for(t in tbl_fav){	

						if(tbl_fav[t].username === data[i].sender){	

							var fav = { "username" : data[i].sender, "profile" : tbl_fav[t].profile, "about" : tbl_fav[t].about, 'status' : "" };
							tbl_fav.splice(t, 1);
							tbl_fav.unshift(fav);
							storage.set("favorites", tbl_fav);
							break;
						}else{
							counter++;
						}

					}
				}else{
					var fav = { "username" : data[i].sender, "profile" : "default.jpg", "about" : "not available", 'status' : "" };
				 	tbl_fav.unshift(fav);
				 	storage.set("favorites", tbl_fav);
				}


				if(counter === tbl_fav.length){
			 		var fav = { "username" : data[i].sender, "profile" : "default.jpg", "about" : "not available", 'status' : "" };
			 		tbl_fav.unshift(fav);
			 		storage.set("favorites", tbl_fav);	
				}

			}


			//Prepend message to the message body if is opened
			if($(".conversation_body").length){

				var _table = $('.view_username').text();
				var new_table = storage.get(_table);

				for(var key in new_table){

		            if("seen" in new_table[key]){
		                var sent = (new_table[key].seen == 0) ? "<img src='img/notSent.png' style='width: 12px' />" : "<img src='img/tick.png' style='width: 12px' />";
		                var not_sent = (table_name[i].seen == 0) ? "sent_pending" : "";

		                HTMLStr += "<div class='conversation_outgoing  " + not_sent + "'>"+
		                     "<div class='outgoing_conversation_cover'>" + new_table[key].message + "</div>" +
		                     "<div class='send-time_send-status'>" + new_table[key].send_time + " " + sent + " </div>" +
		                     "<br style='clear : both' /></div>";            
		                
		            }else if("read" in new_table[key]){
		            	new_table[key].read = 1;
		            	storage.set(_table, new_table);

		                HTMLStr += "<div class='conversation_income'>"+
		                     "<div class='income_conversation_cover'>" + new_table[key].message + "</div>" +
		                     "<div class='receive-time'>" + new_table[key].send_time + "</div>" + 
		                     "<br style='clear : both' /></div>";


		            }
            	}

            	$(".conversation_body").css({'height' : max, 'overflow-y' : 'auto'}).html(HTMLStr);
            }

				setTimeout(server_request(url, params), 3000);
		},

		outgoing : function(receiver, message){

			var currentdate = new Date();
			var uniq_code = currentdate.getUTCMilliseconds();
			var month = Number(currentdate.getMonth()) + 1;
			var month = (month < 10) ? '0' + month : month;
			var day = currentdate.getDate();
			var day = (day < 10) ? '0' + day : day;
			var curr_date = currentdate.getFullYear() + '/' + month + '/' + day;
			var curr_time = format_time(currentdate);
			var table = storage.get(receiver) || [];
			var table_name = receiver.replace(/\s+/g, ''); //remove spaces by underscore
			var tbl_fav = storage.get("favorites") || [];
			var url = "http://www.lovenest.co.za/App_Models/Messages.php?callback=SendMessage";
			var data = { 'action' : 'send', 'sender' : storage.get('username'), 'receiver' : receiver, 'message' : message, 'code' : uniq_code };
			
			var obj = { 
				'sender' : receiver,
				'message' : message,
				'send_time' : curr_time,
				'send_date' : curr_date,
				'code' : uniq_code,
				'seen' : 0
			}

			for(t in tbl_fav){	

				if(tbl_fav[t].username === obj.sender){	

					var fav = { "username" : obj.sender, "profile" : tbl_fav[t].profile, "about" : tbl_fav[t].about, 'status' : "" };
					tbl_fav.splice(t, 1);
					tbl_fav.unshift(fav);
					storage.set("favorites", tbl_fav);

				}

			}


			table.unshift(obj);
			storage.set(table_name, table);

			//Send message to the server
			server_request(url, data);

		}

	}



//Send message
$(document).on("click", ".chat-send-message", function(){
    var text = $(".chat-text-box").val().replace(/<a/gi, "");
    var user = $(".view_username").text();
    var date = new Date();
    var html = "<div class='conversation_outgoing sent_pending'>"+
             "<div class='outgoing_conversation_cover'>" + text + "</div>" +
             "<div class='send-time_send-status'>" + format_time(date) + "</div>" +
             "<br style='clear : both' /></div>";
     
    if(text != ""){
    	$(".startConvo").hide();
    	$(".conversation_body").prepend(html);
    	$(".chat-text-box").val("");
    	messaging.outgoing(user, text);
	      
    }
})

//Send message on enter
$(document).on("keydown", ".chat-text-box", function(e){
    
    if(e.keyCode == 13){
        $(".chat-send-message").trigger("click");
    }

})

//Check new messages after few seconds
var url = "http://www.lovenest.co.za/App_Models/Messages.php?callback=callback";
var data = { 'action' : 'receive', 'receiver' : storage.get('username') };

//Check new messages
//Only check message if user is logged in
if(storage.get('isLogin') === true) {
	setTimeout(server_request(url, data), 1000);
	setTimeout(AutoSendMessage(url), 3000);
}


//Send all pending messages automatically		
function AutoSendMessage(addr){
	
	var tbl_recipt = storage.get('favorites') || [];

	if(tbl_recipt.length > 0){

		for(var i = 0; i < tbl_recipt.length; i++){	
			var user_msg = tbl_recipt[i].username.replace(/\s+/g, '');
			var user_tbl = storage.get(user_msg) || [];

			if(user_tbl.length > 0){

				for(var r = 0; r < user_tbl.length; r++){

					if(user_tbl[r].seen === 0){
						
						var data = {}
						data.action = 'send', 
						data.sender = storage.get('username'), 
						data.receiver = user_tbl[r].sender, 
						data.message = user_tbl[r].message, 
						data.code = user_tbl[r].code
						
						server_request(addr, data);
					}
				}
			}
		}
	  
	}
	

}



//Send emotions message
$(document).on("click", ".addEmoticon", function(){

    var text = "<img src='" + $(this).attr("src") + "' class='sentEmotions' />";
    var user = $(".view_username").text();
    var date = new Date();
    var html = "<div class='conversation_outgoing sent_pending'>"+
             "<div class='outgoing_conversation_cover'>" + text + "</div>" +
             "<div class='send-time_send-status'>" + format_time(date) + " <img src='img/notSent.png' style='width: 12px' /></div>" +
             "<br style='clear : both' /></div>";

    $(".startConvo").hide();        
    $(".conversation_body").prepend(html);   	
    messaging.outgoing(user, text);

})


//Notification beep
function BeepNow() { 

	var sound = storage.get("sound");
    var vibrate = storage.get("vibrate");

  
    if(vibrate === null || vibrate === true){
        navigator.notification.vibrate(100);
    }

    if(sound === null || sound === true){
        navigator.notification.beep();
    }

}

})


//jSpinner is the spinner that is supported in android phonegap
//Developed by Rofhiwa Sikhauli
//Date: 03 February 2015


var jSpinner = {

	elementCreate : function(){

	 	var styleSpinner = $("<div id='spinnerCover' />").css({
			"width" : "180px",
			"height": "36px",
			"position" : "absolute",
			"top" : 0,
			"left" : 0,
			"right" : 0,
			"bottom" : 0,
			"margin-top" : "auto",
			"margin-left" : "auto",
			"margin-right" : "auto",
			"margin-bottom" : "auto",
			"padding" : "2px 4px 2px 4px",
			"border" : "1px solid #DDD",
			"background" : "#DDD",
			"border-radius" : "2px",
			"overflow" : "hidden",
			"text-align" : "left",
			"font-size" : "16px",
			"color" : "#444",
			"font-family" : "verdana",
			"box-shadow" : "0 0 4px #BBB inset",
			"z-index" : 9999999		
		}).html("<img src='img/white_spinner.png' style='width: 30px; margin: 4px 10px 0 4px;display: block; float: left' />" + 
				"<div id='showLoadText' style='width: 134px; height: 20px; float: left; margin-top: 9px;'></div>");

		return styleSpinner;	
	},

	startSpinner : function(textDisplay){

		if(!$("#spinnerCover").length){
			$("html").append(this.elementCreate);

		}

		// alert(textDisplay)	
		var icon = window.sessionStorage.getItem("icons") || 0;
		var icon =  Number(icon) + 10;
		var speed = 10;

		$("#showLoadText").text(textDisplay);
		$("#spinnerCover img").css({ "transform" : "rotate(" + icon + "deg)" });

		window.sessionStorage.setItem("icons", icon);

		setInterval(this.startSpinner, speed);

	},

	stopSpinner : function(){
		$("html").find("#spinnerCover").remove();
		window.sessionStorage.removeItem("icons")
	}

}


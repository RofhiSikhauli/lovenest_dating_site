
$(document).ready(function(){

    //Uploa from gallery
    $(document).on("click", "#uploadFromGallery", function(){
        
        navigator.camera.getPicture(uploadPhoto, openFail,
                            { 
                                quality: 50, 
                                destinationType: navigator.camera.DestinationType.FILE_URI,
                                sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
                                encodingType: Camera.EncodingType.JPEG,
                                correctOrientation: true,
                                targetWidth: 256,
                                targetHeight: 256
                            });

    })

    //Upload from camera
    $(document).on("click", "#uploadFromCamera", function(){
        
        navigator.camera.getPicture(uploadPhoto, openFail,
                            { 
                                quality: 50, 
                                destinationType: navigator.camera.DestinationType.FILE_URI,
                                sourceType: navigator.camera.PictureSourceType.CAMERA,
                                correctOrientation: true,
                                encodingType: Camera.EncodingType.JPEG,
                                targetWidth: 256,
                                targetHeight: 256 
                            });

    })

    //On failed do nothing
    function openFail(){}

    //On success view image crop and then send to the server
    function uploadPhoto(imageURI){

        var tempImage = [];
        var obj = {};
        var height = $(document).height() - 60;

        $('#photoCropperMain img').attr("src", imageURI);
        
        $("#photoCropCover").fadeIn("slow", function(){

            
            $('#photoCropperMain').height(height);           

        })

        obj.imgData = imageURI,
        obj.status = 0  
        tempImage[0] = obj;

        storage.set("tempImage", tempImage);

    }


    //close photo crop pop
    $(document).on("click", "#photoCropperCancel", function(){
        $("#photoCropCover").fadeOut("slow", function(){
            $("#photoCropperMain img").attr("src", "#");
        })
    })


    //Save image after crop
    $(document).on("click", "#photoCropperSave", function(){

        var tempImage = storage.get("tempImage");

        if(tempImage != null){
            tempImage[0].status = 1;
            storage.set("tempImage", tempImage);

            var path = "img/default.jpg";

            $("#viewMyProfile img").attr("src", path);
        }
        
        $("#photoCropCover").fadeOut("slow", function(){
            $("#photoCropperMain").empty().html($("<img />").attr("src", "#"));  
            uploadProfilePicture() // Upload image        
        })
    })



    //Upload image
    function uploadProfilePicture(){   
        
        var tempImage = storage.get("tempImage") || [];
        var params = {};
        
        if(tempImage.length === 0 || tempImage[0].status === 0) return;
        
        params.imgData = tempImage[0].imgData;
        params.user = storage.get("username");

        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = params.imgData.substr(params.imgData.lastIndexOf('/')+1);
        options.mimeType = "image/jpeg";
        options.params = params;

        var url = "http://www.lovenest.co.za/App_Models/Upload_Model.php";
        var ft = new FileTransfer();

        ft.upload(params.imgData, encodeURI(url), uploadSuccess, uploadFail, options);

    }
 

    //Upload image passed
    function uploadSuccess(r) {

        var res = r.response;
        
        if(res !== 0){ 

            var newProfile = "http://www.lovenest.co.za/profile_pics/" + res;
            $("#viewMyProfile img").attr("src", newProfile);
            storage.remove("tempImage");

        }

    }

    //Upload imge failed
    function uploadFail(error) {
        alert("Uploading failed")
        uploadProfilePicture();
    }

    
})